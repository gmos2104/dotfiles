require("gmos2104.options")
require("gmos2104.keymaps")

-- [[ Plugins ]]
-- Install lazy.nvim if not installed already
local lazy_path = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazy_path) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazy_path,
	})
end
vim.opt.rtp:prepend(lazy_path)

-- Plugin definition/configuration
require("lazy").setup("gmos2104/plugins")
