return {
	"nvim-treesitter/nvim-treesitter",
	build = function()
		require("nvim-treesitter.install").update({ with_sync = true })
	end,
	config = function()
		require("nvim-treesitter.configs").setup({
			ensure_installed = {
				"bash",
				"c",
				"cpp",
				"css",
				"go",
				"html",
				"javascript",
				"json",
				"lua",
				"markdown",
				"php",
				"prisma",
				"python",
				"terraform",
				"typescript",
				"vimdoc",
				"vue",
			},
			sync_install = true,
			highlight = {
				enable = true,
			},
			indent = {
				enable = false,
			},
			additional_vim_regex_highlighting = true,
		})
	end,
}
