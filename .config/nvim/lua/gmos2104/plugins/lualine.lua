return {
	"nvim-lualine/lualine.nvim",
	config = function()
		require("lualine").setup({
			options = {
				theme = "auto",
				section_separators = "",
				component_separators = "",
			},
			winbar = {
				lualine_a = { { "filename", path = 1 } },
			},
		})
	end,
}
