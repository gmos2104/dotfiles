local keymaps = require("gmos2104.keymaps")

return {
	"neovim/nvim-lspconfig",
	dependencies = {
		{ "folke/neodev.nvim", opts = {} },
	},
	config = function()
		local on_attach = function(_, buffer)
			keymaps.set_lsp_keybinds(buffer)
		end

		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities.textDocument.completion.completionItem.snippetSupport = true

		local cmp_lsp = require("cmp_nvim_lsp")
		cmp_lsp.default_capabilities(capabilities)

		local lspconfig = require("lspconfig")

		local servers = {
			"clangd",
			"eslint",
			"gopls",
			"htmx",
			"prismals",
			"ruff",
			"tailwindcss",
			"terraformls",
			"tsserver",
			"volar",
		}
		for _, lsp in pairs(servers) do
			local server = lspconfig[lsp]
			server.setup({
				on_attach = on_attach,
				capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {}),
			})
		end

		lspconfig["basedpyright"].setup({
			on_attach = on_attach,
			capabilities = vim.tbl_deep_extend("force", {}, capabilities, lspconfig["basedpyright"].capabilities or {}),
			settings = {
				basedpyright = {
					analysis = {
						typeCheckingMode = "standard",
						reportMissingTypeStubs = false,
					},
				},
			},
		})

		lspconfig["lua_ls"].setup({
			on_attach = on_attach,
			capabilities = vim.tbl_deep_extend("force", {}, capabilities, lspconfig["lua_ls"].capabilities or {}),
			settings = {
				Lua = {
					runtime = {
						version = "LuaJIT",
					},
					diagnostics = {
						disable = { "missing-fields" },
					},
					telemetry = {
						enable = false,
					},
				},
			},
		})
	end,
}
