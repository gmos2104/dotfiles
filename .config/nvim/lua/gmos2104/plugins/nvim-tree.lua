return {
	"nvim-tree/nvim-tree.lua",
	opts = {
		filters = {
			custom = { "^.git$" },
		},
		view = {
			width = 50,
		},
	},
}
