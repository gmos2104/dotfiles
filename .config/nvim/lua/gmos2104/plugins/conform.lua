return {
	"stevearc/conform.nvim",
	opts = {
		format_on_save = function(bufnr)
			local disable_filetypes = { c = true, cpp = true, vue = true }
			return {
				timeout_ms = 500,
				lsp_fallback = not disable_filetypes[vim.bo[bufnr].filetype],
			}
		end,
		formatters_by_ft = {
			css = { { "prettierd", "prettier" } },
			html = { { "prettierd", "prettier" } },
			lua = { "stylua" },
			javascript = { { "prettierd", "prettier" } },
			typescript = { { "prettierd", "prettier" } },
		},
	},
}
