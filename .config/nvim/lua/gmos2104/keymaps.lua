local map = function(mode, keybinds, func, options)
	local extended_options = vim.tbl_extend("keep", {
		noremap = true,
		silent = true,
	}, options)

	vim.keymap.set(mode, keybinds, func, extended_options)
end

map("n", "<A-j>", "<cmd>move +1<cr>", { desc = "Move current line below" })
map("n", "<A-k>", "<cmd>move -2<cr>", { desc = "Move current line above" })
map("n", "<C-f>", "<cmd>NvimTreeToggle<cr>", { desc = "Toggle NvimTree" })
map("n", "<Esc>", "<cmd>nohlsearch<CR>", { desc = "Clear highlight" })

local set_dap_keybinds = function()
	local has_dap, dap = pcall(require, "dap")

	if has_dap then
		local widgets = require("dap.ui.widgets")

		map("n", "<leader>dc", dap.continue, { desc = "DAP: [D]ebug [C]ontinue" })
		map("n", "<leader>dn", dap.step_over, { desc = "DAP: [D]ebug [N]ext line" })
		map("n", "<leader>dsi", dap.step_into, { desc = "DAP: [D]ebug [S]tep [I]nto" })
		map("n", "<leader>dso", dap.step_out, { desc = "DAP: [D]aebug [S]tep [O]ut" })
		map("n", "<leader>db", dap.toggle_breakpoint, { desc = "DAP: [D]ebug toggle [B]reakpoint" })
		map("n", "<leader>dr", dap.repl.open, { desc = "DAP: [D]ebug [R]epl" })
		map("n", "<leader>dh", widgets.hover, { desc = "DAP: [D]ebug [H]over" })
		map("n", "<leader>dp", widgets.preview, { desc = "DAP: [D]ebug [P]review" })
		map("n", "<leader>df", function()
			widgets.centered_float(widgets.frames)
		end, { desc = "DAP: [D]ebug [F]rames" })
		map("n", "<leader>ds", function()
			widgets.centered_float(widgets.scopes)
		end, { desc = "DAP: [D]ebug [S]copes" })
	end
end

local set_lsp_keybinds = function(buffer)
	local has_telescope_builtin, telescope_builtin = pcall(require, "telescope.builtin")

	if has_telescope_builtin then
		map("n", "gd", telescope_builtin.lsp_definitions, { buffer = buffer, desc = "LSP: [G]o to [D]efinitions" })
		map("n", "gr", telescope_builtin.lsp_references, { buffer = buffer, desc = "LSP: [G]o to [R]eferences" })
		map(
			"n",
			"gi",
			telescope_builtin.lsp_implementations,
			{ buffer = buffer, desc = "LSP: [G]o to [I]mplementations" }
		)
		map(
			"n",
			"<leader>D",
			telescope_builtin.lsp_type_definitions,
			{ buffer = buffer, desc = "LSP: Type [D]efinitions" }
		)
		map(
			"n",
			"<leader>ds",
			telescope_builtin.lsp_document_symbols,
			{ buffer = buffer, desc = "LSP: [D]ocument [S]ymbols" }
		)
		map(
			"n",
			"<leader>ws",
			telescope_builtin.lsp_workspace_symbols,
			{ buffer = buffer, desc = "LSP: [W]orkspace [S]ymbols" }
		)
	else
		map("n", "gd", vim.lsp.buf.definition, { buffer = buffer, desc = "LSP: [G]o to [D]efinitions" })
		map("n", "gi", vim.lsp.buf.implementation, { buffer = buffer, desc = "LSP: [G]o to [R]eferences" })
		map("n", "gr", vim.lsp.buf.references, { buffer = buffer, desc = "LSP: [G]o to [I]mplementations" })
		map("n", "<leader>D", vim.lsp.buf.type_definition, { buffer = buffer, desc = "LSP: Type [D]efinitions" })
	end

	map("n", "gD", vim.lsp.buf.declaration, { buffer = buffer, desc = "LSP: [G]o to [D]eclaration" })
	map("n", "<leader>rn", vim.lsp.buf.rename, { buffer = buffer, desc = "LSP: [R]e[N]ame" })
	map("n", "<leader>ca", vim.lsp.buf.code_action, { buffer = buffer, desc = "LSP: [C]ode [A]ctions" })
end

local set_telescope_keybinds = function()
	local has_telescope_builtin, telescope_builtin = pcall(require, "telescope.builtin")

	if has_telescope_builtin then
		map("n", "<leader>sb", telescope_builtin.buffers, { desc = "Telescope: [S]earch [B]uffers" })
		map("n", "<leader>sd", telescope_builtin.diagnostics, { desc = "Telescope: [S]earch [D]iagnostics" })
		map("n", "<leader>sf", telescope_builtin.find_files, { desc = "Telescope: [S]earch [F]iles" })
		map("n", "<leader>sg", telescope_builtin.live_grep, { desc = "Telescope: [S]earch live [G]rep" })
		map("n", "<leader>sh", telescope_builtin.help_tags, { desc = "Telescope: [S]earch [H]elp" })
		map("n", "<leader>sk", telescope_builtin.keymaps, { desc = "Telescope: [S]earch [K]eymaps" })
		map("n", "<leader>sw", telescope_builtin.grep_string, { desc = "Telescope: [S]earch [W]ord" })
	end
end

return {
	set_dap_keybinds = set_dap_keybinds,
	set_lsp_keybinds = set_lsp_keybinds,
	set_telescope_keybinds = set_telescope_keybinds,
}
